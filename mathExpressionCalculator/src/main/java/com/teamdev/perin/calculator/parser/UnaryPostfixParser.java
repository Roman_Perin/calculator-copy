package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.ExpressionReader;
import com.teamdev.perin.calculator.operators.Operator;
import com.teamdev.perin.calculator.operators.unary.AbstractPostfixUnaryOperator;
import com.teamdev.perin.calculator.operators.unary.PostfixUnaryOperatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class UnaryPostfixParser implements MathExpressionParser {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    @Override
    public EvaluationCommand parse(ExpressionReader reader) {

        final PostfixUnaryOperatorFactory unaryOperators = new PostfixUnaryOperatorFactory();
        final String remainingExpression = reader.getRemainingExpression();

        logger.debug("Trying to parse postfix unary operator at the 1-st position "
                     + "of the next expression: {}.", remainingExpression);
        for (String operatorPresentation : unaryOperators.getOperatorPresentationSet()) {

            String stringsBeginningPresentation = "^";
            String regularExpression = stringsBeginningPresentation + operatorPresentation;
            String parsedPresentation = getMatchedString(remainingExpression, regularExpression);

            if (parsedPresentation != null) {
                int length = parsedPresentation.length();
                reader.increaseCursorBy(length);
                final AbstractPostfixUnaryOperator unaryOperator = unaryOperators.getOperator(operatorPresentation);
                unaryOperator.setLength(length);
                logger.debug("Unary postfix operator: {} is parsed. It's length is {}.", unaryOperator, length);

                return createEvaluationCommand(unaryOperator);
            }
        }
        return null;
    }

    private String getMatchedString(String remainingExpression, String regularExpression) {

        Pattern pattern = Pattern.compile(regularExpression);
        Matcher matcher = pattern.matcher(remainingExpression);

        String parsedPresentation = null;
        if (matcher.find()) {
            parsedPresentation = matcher.group();
        }

        return parsedPresentation;
    }

    private EvaluationCommand createEvaluationCommand(final Operator currentOperator) {

        return new EvaluationCommand() {
            @Override
            public void evaluate(EvaluationStack evaluationStack) throws EvaluationException {
                evaluationStack.pushPostfixUnaryOperator(currentOperator);
            }
        };
    }
}
