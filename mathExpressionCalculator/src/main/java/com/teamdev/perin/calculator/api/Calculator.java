package com.teamdev.perin.calculator.api;

public interface Calculator {

    public abstract double evaluate(String expression) throws ParseException;

}
