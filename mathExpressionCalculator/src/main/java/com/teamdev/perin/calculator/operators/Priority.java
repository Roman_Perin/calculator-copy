package com.teamdev.perin.calculator.operators;

public enum Priority {
    ADDITION_LEVEL,        // + -   (binary)
    MULTIPLICATION_LEVEL,  // * / %
    POWERING_LEVEL,        // ^
    UNARY_LEVEL,           // ~ - + (unary)
    }
