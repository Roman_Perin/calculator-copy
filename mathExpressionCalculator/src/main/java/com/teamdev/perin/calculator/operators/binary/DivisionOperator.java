package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.operators.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class DivisionOperator extends AbstractBinaryOperator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    public DivisionOperator(Priority priority) {
        super(priority);
        logger.debug("DivisionOperator is initialized with priority: {}.", priority);
    }

    @Override
    public double evaluate(double leftOperand, double rightOperand) throws EvaluationException {

        if (rightOperand == 0) {
            throw new EvaluationException("Division by zero was detected:" + leftOperand + "/" + rightOperand + ".");
        }

        double result = leftOperand / rightOperand;
        logger.debug("Division operation was evaluated: {}/{}={}.", leftOperand, rightOperand, result);
        return result;
    }
}
