package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.operators.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.teamdev.perin.calculator.operators.Priority.*;
import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;


public class BinaryOperatorFactory {

    private Logger logger = LoggerFactory.getLogger(getCurrentClassName());

    private final Map<String, Operator> operators = new HashMap<String, Operator>() {
        {
            put("+", new PlusOperator(ADDITION_LEVEL));
            put("-", new MinusOperator(ADDITION_LEVEL));
            put("*", new MultiplicationOperator(MULTIPLICATION_LEVEL));
            put("/", new DivisionOperator(MULTIPLICATION_LEVEL));
            put("^", new PoweringOperator(POWERING_LEVEL));
        }
    };

    {
        logger.info("BinaryOperatorFactory is instantiated with next set of operators: {}.", operators.keySet());
    }


    public Set<String> getOperatorPresentationSet() {
        return operators.keySet();
    }

    public Operator getOperator(String operatorRepresentation) {
        return operators.get(operatorRepresentation);
    }
}
