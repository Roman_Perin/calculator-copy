package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.operators.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.teamdev.perin.calculator.operators.Priority.UNARY_LEVEL;
import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class PrefixUnaryOperatorFactory {

    private Logger logger = LoggerFactory.getLogger(getCurrentClassName());

    private Map<String, Operator> prefixOperators = new HashMap<String, Operator>() {
        {
            put("-", new UnaryMinus(UNARY_LEVEL));
            put("+", new UnaryPlus(UNARY_LEVEL));
            put("~", new BitwiseNot(UNARY_LEVEL));
        }
    };

    {
        logger.info("PrefixUnaryOperatorFactory is instantiated "
                    + "with next set of operators: {}.", prefixOperators.keySet());
    }


    public Operator getOperator(String presentation) {
        return prefixOperators.get(presentation);
    }

    public Set<String> getOperatorPresentationSet() {
        return prefixOperators.keySet();
    }
}
