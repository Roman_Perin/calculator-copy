package com.teamdev.perin.calculator;

import com.teamdev.perin.calculator.api.ParseException;
import com.teamdev.perin.stateMachine.StateMachineContext;

public class EvaluationContext implements StateMachineContext<Double, ParseException> {

    private final ExpressionReader expressionReader;
    private EvaluationStack evaluationStack = new EvaluationStack(this);

    public EvaluationContext(String mathExpression) {
        this.expressionReader = new ExpressionReader(mathExpression);
    }

    public ExpressionReader getExpressionReader() {
        return expressionReader;
    }

    public EvaluationStack getEvaluationStack() {
        return evaluationStack;
    }

    public void setEvaluationStack(EvaluationStack current) {
        this.evaluationStack = current;
    }

    @Override
    public Double getResult() throws ParseException {
        return evaluationStack.getResult();
    }

}
