package com.teamdev.perin.calculator.api;

import com.teamdev.perin.calculator.EvaluationContext;
import com.teamdev.perin.calculator.MathExpressionCalculator;

public class CalculatorImpl implements Calculator {

    public double evaluate(String expression) throws ParseException {
        final EvaluationContext context = new EvaluationContext(expression);
        return new MathExpressionCalculator().run(context);
    }
}
