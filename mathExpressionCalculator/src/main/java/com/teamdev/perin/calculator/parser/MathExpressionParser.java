package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.ExpressionReader;

public interface MathExpressionParser {

    /**
     * Returns null if nothing was parsed.
     */
    EvaluationCommand parse(ExpressionReader reader);

}
