package com.teamdev.perin.calculator;

public class EvaluationException extends Exception {

    public EvaluationException(String message) {
        super(message);
    }
}
