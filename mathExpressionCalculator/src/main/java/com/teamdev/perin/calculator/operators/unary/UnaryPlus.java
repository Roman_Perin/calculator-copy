package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.operators.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class UnaryPlus extends AbstractUnaryOperator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    public UnaryPlus(Priority priority) {
        super(priority);
    }

    @Override
    public double evaluate(double operand) {
        double result = +(long) operand;
        logger.debug("Unary plus operation was evaluated: +{}={}.", operand, result);
        return result;
    }
}
