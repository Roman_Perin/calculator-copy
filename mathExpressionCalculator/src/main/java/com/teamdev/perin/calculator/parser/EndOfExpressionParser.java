package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.ExpressionReader;
import com.teamdev.perin.calculator.api.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;


public class EndOfExpressionParser implements MathExpressionParser {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    @Override
    public EvaluationCommand parse(ExpressionReader reader) {

        logger.debug("Trying to parse end of next expression: {}. Cursor is at position: {}.",
                     reader.getExpression(), reader.getCursor());
        if (reader.getExpression().length() == reader.getCursor()) {
            logger.debug("End of expression was found.");
            return createEvaluationCommand(reader);
        }
        return null;
    }

    private EvaluationCommand createEvaluationCommand(final ExpressionReader reader) {

        return new EvaluationCommand() {
            @Override
            public void evaluate(EvaluationStack evaluationStack) throws ParseException, EvaluationException {
                evaluationStack.pushEndOfExpression();
            }
        };

    }
}
