package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.ExpressionReader;
import com.teamdev.perin.calculator.api.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class DelimiterParser implements MathExpressionParser {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    @Override
    public EvaluationCommand parse(ExpressionReader reader) {

        final String remainingExpression = reader.getRemainingExpression();
        final String delimiterPresentation = ",";

        logger.debug("Trying to parse delimiter \',\' at 1-st position of: {}.", remainingExpression);
        if (remainingExpression.startsWith(delimiterPresentation)) {
            reader.increaseCursorBy(delimiterPresentation.length());
            logger.debug("Delimiter ',' was parsed.");
            return createEvaluationCommand();
        }

        return null;
    }


    private EvaluationCommand createEvaluationCommand() {

        return new EvaluationCommand() {
            @Override
            public void evaluate(EvaluationStack evaluationStack) throws EvaluationException, ParseException {
                evaluationStack.pushDelimiter();
            }
        };
    }
}
