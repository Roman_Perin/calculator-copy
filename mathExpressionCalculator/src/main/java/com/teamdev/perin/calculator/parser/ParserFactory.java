package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static com.teamdev.perin.calculator.State.*;
import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class ParserFactory {

    private Logger logger = LoggerFactory.getLogger(getCurrentClassName());

    private final Map<State, MathExpressionParser> parsers = new HashMap<State, MathExpressionParser>() {
        {
            put(NUMBER, new NumberParser());
            put(BINARY_OPERATOR, new BinaryOperatorParser());
            put(FINISH, new EndOfExpressionParser());
            put(LEFT_BRACKET, new LeftBracketParser());
            put(RIGHT_BRACKET, new RightBracketParser());
            put(UNARY_PREFIX_OPERATOR, new UnaryPrefixParser());
            put(UNARY_POSTFIX_OPERATOR, new UnaryPostfixParser());
            put(FUNCTION, new FunctionParser());
            put(DELIMITER, new DelimiterParser());
        }
    };

    {
        logger.debug("ParseFactory is instantiated with next set of parsers: {}.", parsers.keySet());
    }


    public MathExpressionParser getParser(State state) {

        MathExpressionParser parser = parsers.get(state);
        if (parser == null) {
            throw new IllegalStateException("Parser wasn't found for state: " + state + ".");
        }

        return parser;
    }

}
