package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.ExpressionReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;


public class LeftBracketParser implements MathExpressionParser {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    @Override
    public EvaluationCommand parse(ExpressionReader reader) {

        final String remainingExpression = reader.getRemainingExpression();
        final String leftBracketPresentation = "(";

        logger.debug("Trying to parse left bracket at 1-st position of: {}.", remainingExpression);
        if (remainingExpression.startsWith(leftBracketPresentation)) {
            reader.increaseCursorBy(leftBracketPresentation.length());
            logger.debug("Left bracket was parsed.");
            return createEvaluationCommand();
        }

        return null;
    }


    private EvaluationCommand createEvaluationCommand() {

        return new EvaluationCommand() {
            @Override
            public void evaluate(EvaluationStack evaluationStack) {
                evaluationStack.pushLeftBracket();
            }
        };
    }
}
