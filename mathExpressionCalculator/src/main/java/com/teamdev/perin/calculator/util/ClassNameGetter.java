package com.teamdev.perin.calculator.util;

public class ClassNameGetter {

    public static String getCurrentClassName() {
        int previousStackFrameIndex = 1; //Index of Stack Frame from which current method was called
        try {
            throw new RuntimeException();
        } catch (RuntimeException e) {
            return e.getStackTrace()[previousStackFrameIndex].getClassName();
        }
    }

}
