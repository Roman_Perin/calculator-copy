package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.operators.Priority;
import com.teamdev.perin.calculator.operators.binary.BinaryOperatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class UnaryMinus extends AbstractUnaryOperator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    public UnaryMinus(Priority priority) {
        super(priority);
    }

    @Override
    public double evaluate(double operand) {
        double result = -(long) operand;
        logger.debug("Unary minus operation was evaluated: -{}={}.", operand, result);
        return result;
    }

}
