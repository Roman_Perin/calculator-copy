package com.teamdev.perin.calculator;

public class ExpressionReader {

    final private String expression;
    private int cursor = 0;

    public ExpressionReader(String expression) {
        this.expression = expression;
    }

    public String getExpression() {
        skipWhitespace();
        return expression;
    }

    public String getRemainingExpression() {
        skipWhitespace();
        return expression.substring(cursor);
    }

    public char getCurrentChar() {
        return expression.charAt(cursor);
    }

    public int getCursor() {
        return cursor;
    }

    public void setCursor(int cursor) {
        this.cursor = cursor;
    }

    public void increaseCursorBy(int delta) {
        cursor += delta;
    }

    private void skipWhitespace() {
        while (cursor < expression.length() && Character.isWhitespace(getCurrentChar())) {
            cursor++;
        }
    }

}
