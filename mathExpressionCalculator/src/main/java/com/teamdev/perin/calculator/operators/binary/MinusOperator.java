package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.operators.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class MinusOperator extends AbstractBinaryOperator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    public MinusOperator(Priority priority) {
        super(priority);
        logger.debug("MinusOperator is initialized with priority: {}.", priority);
    }

    @Override
    public double evaluate(double leftOperand, double rightOperand) {
        double result = leftOperand - rightOperand;
        logger.debug("Minus operation was evaluated: {}-{}={}.", leftOperand, rightOperand, result);
        return result;
    }


}
