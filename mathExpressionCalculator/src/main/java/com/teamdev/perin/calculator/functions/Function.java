package com.teamdev.perin.calculator.functions;

import com.teamdev.perin.calculator.EvaluationException;

public interface Function {

    double evaluate(Double... args) throws EvaluationException;

}
