package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.operators.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class Factorial extends AbstractPostfixUnaryOperator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    public Factorial(Priority priority) {
        super(priority);
    }

    @Override
    public double evaluate(double operand) throws EvaluationException {

        if (Math.floor(operand) != operand || operand < 0) {
            throw new EvaluationException("Factorial argument must be non-negative integer, "
                                          + "but " + operand + " was found.");
        }

        double result = operand;
        while (operand > getLength()) {
            operand -= getLength();
            result *= operand;
        }
        logger.debug("{}-factorial operation was evaluated. Operand is: {}, "
                     + "and result is: {}.",getLength(), operand, result);

        return result;
    }
}
