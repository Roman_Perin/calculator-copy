package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.operators.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class PlusOperator extends AbstractBinaryOperator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    public PlusOperator(Priority priority) {
        super(priority);
        logger.debug("PlusOperator is initialized with priority: {}.", priority);
    }

    @Override
    public double evaluate(double leftOperand, double rightOperand) {
        double result = leftOperand + rightOperand;
        logger.debug("Plus operation was evaluated: {}+{}={}.", leftOperand, rightOperand, result);
        return result;
    }

}
