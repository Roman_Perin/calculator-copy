package com.teamdev.perin.calculator;

public enum State {
    START,
    NUMBER,
    FUNCTION,
    UNARY_PREFIX_OPERATOR,
    UNARY_POSTFIX_OPERATOR,
    BINARY_OPERATOR,
    LEFT_BRACKET,
    RIGHT_BRACKET,
    DELIMITER,
    FINISH,
}
