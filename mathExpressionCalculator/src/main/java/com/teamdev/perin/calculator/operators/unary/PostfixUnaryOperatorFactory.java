package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.operators.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class PostfixUnaryOperatorFactory {

    private Logger logger = LoggerFactory.getLogger(getCurrentClassName());

    private Map<String, AbstractPostfixUnaryOperator> operators
            = new HashMap<String, AbstractPostfixUnaryOperator>() {
        {
//          Put regular expression as a key of operator's representation.
            put("!+", new Factorial(Priority.UNARY_LEVEL));
        }
    };

    {
        logger.info("PostfixUnaryOperatorFactory is instantiated with "
                    + "next set of operators: {}.", operators.keySet());
    }


    public AbstractPostfixUnaryOperator getOperator(String presentation) {
        return operators.get(presentation);
    }

    public Set<String> getOperatorPresentationSet() {
        return operators.keySet();
    }
}
