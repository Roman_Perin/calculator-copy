package com.teamdev.perin.calculator;

import com.teamdev.perin.stateMachine.TransitionMatrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static com.teamdev.perin.calculator.State.*;
import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;
import static java.util.EnumSet.*;

public class TransitionMatrixImpl implements TransitionMatrix<State> {

    private Logger logger = LoggerFactory.getLogger(getCurrentClassName());

    private final Map<State, Set<State>> transitions = new HashMap<State, Set<State>>() {
        {
            put(START, of(NUMBER, LEFT_BRACKET, UNARY_PREFIX_OPERATOR, FUNCTION));
            put(NUMBER, of(BINARY_OPERATOR, FINISH, RIGHT_BRACKET, UNARY_POSTFIX_OPERATOR, DELIMITER));
            put(FUNCTION, of(LEFT_BRACKET));
            put(FINISH, noneOf(State.class));
        }
    };

    {
        transitions.put(UNARY_PREFIX_OPERATOR, transitions.get(START));
        transitions.put(DELIMITER, transitions.get(START));
        transitions.put(BINARY_OPERATOR, transitions.get(START));
        transitions.put(LEFT_BRACKET, of(RIGHT_BRACKET, transitions.get(START).toArray(new State[0])));
        transitions.put(UNARY_POSTFIX_OPERATOR, transitions.get(NUMBER));
        transitions.put(RIGHT_BRACKET, transitions.get(NUMBER));
        logger.info("TransitionMatrixImpl is instantiated with next set of transitions: {}.", transitions);
    }


    @Override
    public State getStartState() {
        return START;
    }

    @Override
    public State getFinishState() {
        return FINISH;
    }

    @Override
    public Set<State> getPossibleStates(State state) {
        Set<State> sortedStates = new TreeSet<State>();
        sortedStates.addAll(transitions.get(state));
        return sortedStates;
    }
}
