package com.teamdev.perin.calculator.functions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class FunctionFactory {

    private Logger logger = LoggerFactory.getLogger(getCurrentClassName());

    private final Map<String, Function> functions = new HashMap<String, Function>() {
        {
            put("max", new Max());
            put("min", new Min());
            put("sum", new Sum());
            put("sqrt", new Sqrt());
        }
    };

    {
        logger.info("FunctionFactory is instantiated with next set of functions: {}", functions.keySet());
    }


    public Function getFunction(String presentation) {
        return functions.get(presentation);
    }

    public Set<String> getFunctionPresentationSet() {
        return functions.keySet();
    }

}
