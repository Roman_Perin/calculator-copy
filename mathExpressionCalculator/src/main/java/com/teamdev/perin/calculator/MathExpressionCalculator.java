package com.teamdev.perin.calculator;

import com.teamdev.perin.stateMachine.FiniteStateMachine;
import com.teamdev.perin.calculator.api.ParseException;
import com.teamdev.perin.stateMachine.StateAcceptor;
import com.teamdev.perin.stateMachine.TransitionMatrix;

public class MathExpressionCalculator extends FiniteStateMachine<Double, State, EvaluationContext, ParseException> {


    @Override
    protected void deadlock(State state, EvaluationContext context) throws ParseException {
        throw new ParseException("Math elements sequence is illegal.", context.getExpressionReader().getCursor());
    }

    @Override
    protected TransitionMatrix<State> getTransitionMatrix() {
        return new TransitionMatrixImpl();
    }

    @Override
    protected StateAcceptor<Double, State, EvaluationContext, ParseException> createStateAcceptor() {
        return new StateAcceptorImpl();
    }

}
