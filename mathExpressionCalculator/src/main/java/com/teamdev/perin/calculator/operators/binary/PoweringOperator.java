package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.operators.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class PoweringOperator extends AbstractBinaryOperator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    public PoweringOperator(Priority priority) {
        super(priority);
        logger.debug("PoweringOperator is initialized with priority: {}.", priority);
    }

    @Override
    public double evaluate(double leftOperand, double rightOperand) {
        double result = Math.pow(leftOperand, rightOperand);
        logger.debug("Powering operation was evaluated: {}^{}={}.", leftOperand, rightOperand, result);
        return result;
    }
}
