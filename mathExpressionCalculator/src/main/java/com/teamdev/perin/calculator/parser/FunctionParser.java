package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.ExpressionReader;
import com.teamdev.perin.calculator.functions.Function;
import com.teamdev.perin.calculator.functions.FunctionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;


public class FunctionParser implements MathExpressionParser {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    @Override
    public EvaluationCommand parse(ExpressionReader reader) {

        final FunctionFactory functions = new FunctionFactory();
        final String remainingExpression = reader.getRemainingExpression();

        logger.debug("Trying to parse function at the 1-st position of the next expression: {}.", remainingExpression);
        for (String functionPresentation : functions.getFunctionPresentationSet()) {

            if (remainingExpression.startsWith(functionPresentation)) {
                reader.increaseCursorBy(functionPresentation.length());
                final Function function = functions.getFunction(functionPresentation);
                logger.debug("Function: {} is parsed.", function);

                return createEvaluationCommand(function);
            }
        }

        return null;
    }


    private EvaluationCommand createEvaluationCommand(final Function currentFunction) {

        return new EvaluationCommand() {
            @Override
            public void evaluate(EvaluationStack evaluationStack) {
                evaluationStack.pushFunction(currentFunction);
            }
        };
    }

}
