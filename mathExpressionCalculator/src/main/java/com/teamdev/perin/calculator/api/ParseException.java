package com.teamdev.perin.calculator.api;

public class ParseException extends Exception {

    private int errorPosition;

    public ParseException(String message) {
        super(message);
    }

    public ParseException(String message, int errorPosition) {
        super(message);
        this.errorPosition = errorPosition;
    }

    public int getErrorPosition() {
        return errorPosition;
    }

    public void setErrorPosition(int errorPosition){
        this.errorPosition = errorPosition;
    }
}
