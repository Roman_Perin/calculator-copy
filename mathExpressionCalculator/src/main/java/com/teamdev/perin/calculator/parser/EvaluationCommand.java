package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.api.ParseException;

public interface EvaluationCommand {

   public abstract void evaluate(EvaluationStack context) throws ParseException, EvaluationException;

}
