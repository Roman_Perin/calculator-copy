package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.ExpressionReader;
import com.teamdev.perin.calculator.api.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class RightBracketParser implements MathExpressionParser {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());

    @Override
    public EvaluationCommand parse(ExpressionReader reader) {

        final String remainingExpression = reader.getRemainingExpression();
        final String rightBracketPresentation = ")";
        logger.debug("Trying to parse right bracket at 1-st position of the next expression: {}.", remainingExpression);

        if (remainingExpression.startsWith(rightBracketPresentation)) {
            logger.debug("Right bracket was parsed.");
            reader.increaseCursorBy(rightBracketPresentation.length());
            return createEvaluationCommand(reader);
        }

        return null;
    }


    private EvaluationCommand createEvaluationCommand(final ExpressionReader reader) {

        return new EvaluationCommand() {
            @Override
            public void evaluate(EvaluationStack evaluationStack) throws ParseException, EvaluationException {

                evaluationStack.pushRightBracket();

            }
        };
    }
}
