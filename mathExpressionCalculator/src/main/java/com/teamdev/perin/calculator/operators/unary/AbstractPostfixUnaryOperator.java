package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.operators.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public abstract class AbstractPostfixUnaryOperator extends AbstractUnaryOperator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());
    private int length;

    public AbstractPostfixUnaryOperator(Priority priority) {
        super(priority);
    }

    public void setLength(int length) {
        logger.debug("Postfix unary operator's length is initiating with {}.", length);
        this.length = length;
    }

    public int getLength() {
        return length;
    }
}
