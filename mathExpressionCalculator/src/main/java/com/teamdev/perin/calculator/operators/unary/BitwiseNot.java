package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.operators.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class BitwiseNot extends AbstractUnaryOperator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    public BitwiseNot(Priority priority) {
        super(priority);
    }

    @Override
    public double evaluate(double operand) throws EvaluationException {

        if (Math.floor(operand) != operand) {
            throw new EvaluationException("Bitwise NOT argument must be integer, but double " + operand + " was found.");
        }

        double result = ~(long) operand;
        logger.debug("Bitwise NOT operation was evaluated: ~{}={}.", operand, result);

        return result;
    }
}
