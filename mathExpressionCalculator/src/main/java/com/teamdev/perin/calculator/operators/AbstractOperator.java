package com.teamdev.perin.calculator.operators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public abstract class AbstractOperator implements Operator {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());
    private Priority priority;

    public AbstractOperator(Priority priority) {
        this.priority = priority;
    }

    @Override
    public int compareTo(Operator o) {
        AbstractOperator other = (AbstractOperator) o;
        int result = this.priority.compareTo(other.priority);
        logger.debug("Two operators were compared with priorities: {} and {}."
                     + "Comparing result is: {}.", this.priority, other.priority, result);
        return result;
    }

}
