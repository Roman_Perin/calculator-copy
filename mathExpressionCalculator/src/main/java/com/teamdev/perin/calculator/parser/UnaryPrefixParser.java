package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.ExpressionReader;
import com.teamdev.perin.calculator.operators.Operator;
import com.teamdev.perin.calculator.operators.unary.PrefixUnaryOperatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;


public class UnaryPrefixParser implements MathExpressionParser {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    @Override
    public EvaluationCommand parse(ExpressionReader reader) {

        final PrefixUnaryOperatorFactory unaryOperators = new PrefixUnaryOperatorFactory();
        final String remainingExpression = reader.getRemainingExpression();

        logger.debug("Trying to parse prefix unary operator at the 1-st position "
                     + "of the next expression: {}.", remainingExpression);
        for (String operatorPresentation : unaryOperators.getOperatorPresentationSet()) {

            if (remainingExpression.startsWith(operatorPresentation)) {
                reader.increaseCursorBy(operatorPresentation.length());
                final Operator unaryOperator = unaryOperators.getOperator(operatorPresentation);
                logger.debug("UnaryPrefixOperator: {} was parsed.", unaryOperator);

                return createEvaluationCommand(unaryOperator);
            }
        }
        return null;
    }

    private EvaluationCommand createEvaluationCommand(final Operator currentOperator) {

        return new EvaluationCommand() {
            @Override
            public void evaluate(EvaluationStack evaluationStack) {
                evaluationStack.pushPrefixUnaryOperator(currentOperator);
            }
        };
    }

}
