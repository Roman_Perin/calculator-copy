package com.teamdev.perin.calculator;

import com.teamdev.perin.calculator.parser.EvaluationCommand;
import com.teamdev.perin.calculator.parser.MathExpressionParser;
import com.teamdev.perin.calculator.parser.ParserFactory;
import com.teamdev.perin.calculator.api.ParseException;
import com.teamdev.perin.stateMachine.StateAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;


public class StateAcceptorImpl implements StateAcceptor<Double, State, EvaluationContext, ParseException> {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    @Override
    public boolean accept(State state, EvaluationContext context) throws ParseException {

        logger.debug("Trying to move to the state: {}.", state);
        ParserFactory parsers = new ParserFactory();
        final MathExpressionParser parser = parsers.getParser(state);

        final EvaluationCommand command = parser.parse(context.getExpressionReader());
        if (command == null) {
            logger.debug("Impossible to move to the state: {}.", state);
            return false;
        }

        logger.debug("Moving to the state {} is possible, trying to do evaluations.", state);
        try {
            command.evaluate(context.getEvaluationStack());
        } catch (EvaluationException e) {
            throw new ParseException(e.getMessage(), context.getExpressionReader().getCursor());
        }

        return true;
    }

}
