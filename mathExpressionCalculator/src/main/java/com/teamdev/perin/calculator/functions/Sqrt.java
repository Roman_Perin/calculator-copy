package com.teamdev.perin.calculator.functions;

import com.teamdev.perin.calculator.EvaluationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class Sqrt implements Function {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());

    @Override
    public double evaluate(Double... args) throws EvaluationException {
        int allowedArgumentNumber = 1;

        if (args.length != allowedArgumentNumber) {
            throw new EvaluationException("Sqrt requires only 1 argument, but " + args.length + " were given.");
        }
        if (args[0] < 0) {
            throw new EvaluationException("Sqrt arguments must be non-negative, but " + args[0] + " was given.");
        }

        double result = Math.sqrt(args[0]);
        logger.debug("Sqrt function was evaluated: sqrt({})={}.", args[0], result);

        return result;
    }
}
