package com.teamdev.perin.calculator.parser;


import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.ExpressionReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;


public class NumberParser implements MathExpressionParser {

    public static final NumberFormat NUMBER_FORMAT = new DecimalFormat("0.0");
    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    @Override
    public EvaluationCommand parse(ExpressionReader reader) {

        logger.debug("Trying to parse number. Expression is: {}. Cursor is at position: {}.",
                     reader.getExpression(), reader.getCursor());

        final ParsePosition parsePosition = new ParsePosition(reader.getCursor());
        final Number number = NUMBER_FORMAT.parse(reader.getExpression(), parsePosition);

        if (parsePosition.getErrorIndex() > -1) {
            return null;
        }

        final double result = number.doubleValue();
        int newPosition = parsePosition.getIndex();
        reader.setCursor(newPosition);
        logger.debug("Number was parsed: {}. New cursor's position is: {}.", result, newPosition);

        return new EvaluationCommand() {
            @Override
            public void evaluate(EvaluationStack evaluationStack) throws EvaluationException {
                evaluationStack.pushNumber(result);
            }
        };
    }
}
