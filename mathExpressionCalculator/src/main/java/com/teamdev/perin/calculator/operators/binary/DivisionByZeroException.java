package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.api.ParseException;

public class DivisionByZeroException extends ParseException {

    public DivisionByZeroException(String message) {
        super(message);
    }
}
