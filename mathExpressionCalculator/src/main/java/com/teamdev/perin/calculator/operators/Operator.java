package com.teamdev.perin.calculator.operators;

import com.teamdev.perin.calculator.EvaluationException;


public interface Operator extends Comparable<Operator> {

    double evaluateOperator(double... operands) throws EvaluationException;

    int getNumberOfArguments();

}
