package com.teamdev.perin.calculator.parser;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.EvaluationStack;
import com.teamdev.perin.calculator.ExpressionReader;
import com.teamdev.perin.calculator.operators.Operator;
import com.teamdev.perin.calculator.operators.binary.BinaryOperatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;


public class BinaryOperatorParser implements MathExpressionParser {

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());


    @Override
    public EvaluationCommand parse(ExpressionReader reader) {

        final BinaryOperatorFactory operators = new BinaryOperatorFactory();
        final String remainingExpression = reader.getRemainingExpression();

        logger.debug("Trying to parse binary operator at the 1-st position of: {}.", remainingExpression);
        for (String operatorPresentation : operators.getOperatorPresentationSet()) {

            if (remainingExpression.startsWith(operatorPresentation)) {
                reader.increaseCursorBy(operatorPresentation.length());
                final Operator binaryOperator = operators.getOperator(operatorPresentation);
                logger.debug("Binary operator: {} was parsed.", binaryOperator);

                return createEvaluationCommand(binaryOperator);
            }
        }

        return null;
    }


    private EvaluationCommand createEvaluationCommand(final Operator currentOperator) {

        return new EvaluationCommand() {
            @Override
            public void evaluate(EvaluationStack evaluationStack) throws EvaluationException {
                evaluationStack.pushBinaryOperator(currentOperator);
            }
        };
    }

}
