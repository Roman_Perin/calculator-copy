package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import com.teamdev.perin.calculator.operators.Priority;
import org.junit.Before;
import org.junit.Test;

import static com.teamdev.perin.calculator.operators.Priority.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MinusOperatorTest {

    private MinusOperator minus;
    private Priority priority;

    @Before
    public void setUp()  {
        priority = ADDITION_LEVEL;
        minus = new MinusOperator(priority);
    }

    @Test
    public void testMinusEvaluation(){
        double leftOperand = 10;
        double rightOperand = 7;
        double inaccuracy = 1e-10;
        double expected = 3d;
        assertEquals("Minus operator evaluates incorrectly.", expected, minus.evaluate(leftOperand, rightOperand), inaccuracy);
    }

    @Test
    public void testMinusExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "10-2";
        double expected = 8;
        double delta = 1e-100;
        assertEquals("Minus expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testMinusCompareToItself() {
        MinusOperator otherMinus = new MinusOperator(priority);
        assertTrue("Something is wrong with minus comparing with it self." ,0 == minus.compareTo(otherMinus));
    }

    @Test
    public void testMinusCompareToMultiplication() {
        Priority multiplicationPriority = MULTIPLICATION_LEVEL;
        MultiplicationOperator multiply = new MultiplicationOperator(multiplicationPriority);
        assertTrue("Something is wrong with minus and multiplication comparing." ,0 > minus.compareTo(multiply));
        assertTrue("Something is wrong with minus and multiplication comparing." ,0 < multiply.compareTo(minus));
    }

    @Test
    public void testMinusCompareToPlus() {
        Priority plusPriority = ADDITION_LEVEL;
        PlusOperator multiply = new PlusOperator(plusPriority);
        assertTrue("Something is wrong with minus and multiply comparing." ,0 == minus.compareTo(multiply));
        assertTrue("Something is wrong with minus and multiply comparing." ,0 == multiply.compareTo(minus));
    }

    @Test
    public void testMinusCompareToDivision() {
        Priority divisionPriority = MULTIPLICATION_LEVEL;
        DivisionOperator division = new DivisionOperator(divisionPriority);
        assertTrue("Something is wrong with minus and division comparing." ,0 > minus.compareTo(division));
        assertTrue("Something is wrong with minus and division comparing." ,0 < division.compareTo(minus));
    }

    @Test
    public void testMinusCompareToPowering() {
        Priority poweringPriority = POWERING_LEVEL;
        PoweringOperator powering = new PoweringOperator(poweringPriority);
        assertTrue("Something is wrong with minus and powering comparing." ,0 > minus.compareTo(powering));
        assertTrue("Something is wrong with minus and powering comparing." ,0 < powering.compareTo(minus));
    }

}
