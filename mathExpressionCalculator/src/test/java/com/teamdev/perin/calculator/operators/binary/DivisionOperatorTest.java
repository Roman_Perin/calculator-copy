package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.EvaluationException;
import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import com.teamdev.perin.calculator.operators.Priority;
import org.junit.Before;
import org.junit.Test;

import static com.teamdev.perin.calculator.operators.Priority.MULTIPLICATION_LEVEL;
import static com.teamdev.perin.calculator.operators.Priority.POWERING_LEVEL;
import static org.junit.Assert.*;

public class DivisionOperatorTest {

    private DivisionOperator division;
    private Priority priority;

    @Before
    public void setUp()  {
        priority = MULTIPLICATION_LEVEL;
        division = new DivisionOperator(priority);
    }

    @Test
    public void testDivisionEvaluation() throws EvaluationException {
        double leftOperand = 10;
        double rightOperand = 2;
        double inaccuracy = 1e-10;
        double expected = 5d;
        assertEquals("Division operator evaluates incorrectly.", expected, division.evaluate(leftOperand, rightOperand), inaccuracy);
    }

    @Test
    public void testDivisionExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "10/2";
        double expected = 5;
        double delta = 1e-100;
        assertEquals("Division expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test (expected = EvaluationException.class)
    public void testDivisionByZero() throws EvaluationException {
        division.evaluate(1, 0);
        fail("Division by zero is invalid. DivisionByZeroException was expecting. But any exception was thrown.");
    }

    @Test
    public void testDivisionCompareToItself() {
        DivisionOperator otherDivision = new DivisionOperator(priority);
        assertTrue("Something is wrong with division comparing.", 0 == division.compareTo(otherDivision));
    }

    @Test
    public void testDivisionCompareToPowering() {
        Priority poweringPriority = POWERING_LEVEL;
        PoweringOperator powering = new PoweringOperator(poweringPriority);
        assertTrue("Something is wrong with division and powering comparing.", 0 > division.compareTo(powering));
        assertTrue("Something is wrong with division and powering comparing.", 0 < powering.compareTo(division));
    }

}
