package com.teamdev.perin.calculator;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Before;
import org.junit.Test;

import static java.lang.Double.doubleToLongBits;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CalculatorImplTest {

    private CalculatorImpl calculator;

    @Before
    public void setUp() {
        calculator = new CalculatorImpl();
    }

    @Test (expected = ParseException.class)
    public void testOnEmptyString() throws ParseException {
        String expression = "";
        calculator.evaluate(expression);
        fail("Exception must to be. Empty string is invalid.");
    }

    @Test
    public void testWhitespaceSkipping() throws ParseException {
        String expression = " 1 + 2 - 3 * 2\n";
        double expected = -3;
        double delta = 1e-10;
        try {
            assertEquals("Whitespace skipping work incorrectly.", expected, calculator.evaluate(expression), delta);
        } catch (Exception e) {
            fail("Whitespace skipping don't work.");
        }
    }

    @Test (expected = ParseException.class)
    public void testOnUnknownSign() throws ParseException {
        String expression = "incorrect expression";
        calculator.evaluate(expression);
        fail("Must be exception - unknown signs don't have to have default value");
    }

    @Test (expected = ParseException.class)
    public void testOnNaN() throws ParseException {
        String expression = "10000000^1000000-10000000^1000000";
        calculator.evaluate(expression);
        fail("Exception must to be. NaN is invalid.");
    }

    @Test (expected = ParseException.class)
    public void testOnInfinity() throws ParseException {
        String expression = "10000000^1000000";
        calculator.evaluate(expression);
        fail("Exception must to be. Infinity is invalid.");
    }

    @Test (expected = ParseException.class)
    public void testDivisionByZero() throws ParseException {
        String expression = "1/(min(0,2))";
        calculator.evaluate(expression);
        fail("Exception must to be. Division by zero is invalid.");
    }

    @Test
    public void testExceptionPosition() throws ParseException {
        String expression = "1+2min(4,2)";
        int cursorPosition = 3;
        try {
            calculator.evaluate(expression);
        } catch (ParseException e) {
            assertEquals("Cursor Position specified incorrect", cursorPosition, e.getErrorPosition());
        }
    }

    @Test
    public void testComplexExpression() throws ParseException {
        String expression = "(2+3)*12*6/3^2+1";
        double result = 41;
        assertEquals("Complex test failed: invalid calculation", doubleToLongBits(result), doubleToLongBits(calculator.evaluate(expression)));
    }

    @Test
    public void testVeryComplexException() throws ParseException{
        String expression="sqrt(max(4+1,min(2,8),9))+sum(min(4,min(2,8),5),max(2*(3!+5),11)/~2+4,13*4+5)";

        try {
            calculator.evaluate(expression);
        } catch (ParseException e) {
            fail("Something is wrong with the function processing.\nDetails: " + e.getMessage()
                 + ". Error at " + "position: " + e.getErrorPosition() + ".");
        }
    }

}
