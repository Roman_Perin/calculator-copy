package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import com.teamdev.perin.calculator.operators.Priority;
import org.junit.Before;
import org.junit.Test;

import static com.teamdev.perin.calculator.operators.Priority.MULTIPLICATION_LEVEL;
import static com.teamdev.perin.calculator.operators.Priority.POWERING_LEVEL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MultiplicationOperatorTest {

    private MultiplicationOperator multiplication;
    private Priority priority;

    @Before
    public void setUp()  {
        priority = MULTIPLICATION_LEVEL;
        multiplication = new MultiplicationOperator(priority);
    }

    @Test
    public void testMultiplicationEvaluation() {
        double leftOperand = 10;
        double rightOperand = 2;
        double inaccuracy = 1e-10;
        double expected = 20d;
        assertEquals("Multiplication operator evaluates incorrectly.", expected, multiplication.evaluate(leftOperand, rightOperand), inaccuracy);
    }

    @Test
    public void testMultiplicationExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "10*2";
        double expected = 20;
        double delta = 1e-100;
        assertEquals("Multiplication expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testMultiplicationCompareToItself() {
        MultiplicationOperator otherMultiplication = new MultiplicationOperator(priority);
        assertTrue("Something is wrong with multiplication comparing.", 0 == multiplication.compareTo(otherMultiplication));
    }

    @Test
    public void testMultiplicationCompareToDivision() {
        Priority divisionPriority = MULTIPLICATION_LEVEL;
        DivisionOperator division = new DivisionOperator(divisionPriority);
        assertTrue("Something is wrong with multiplication and division comparing." ,0 == multiplication.compareTo(division));
        assertTrue("Something is wrong with multiplication and division comparing.", 0 == division.compareTo(multiplication));
    }

    @Test
    public void testMultiplicationCompareToPowering() {
        Priority poweringPriority = POWERING_LEVEL;
        PoweringOperator powering = new PoweringOperator(poweringPriority);
        assertTrue("Something is wrong with multiplication and powering comparing.", 0 > multiplication.compareTo(powering));
        assertTrue("Something is wrong with multiplication and powering comparing.", 0 < powering.compareTo(multiplication));
    }
}
