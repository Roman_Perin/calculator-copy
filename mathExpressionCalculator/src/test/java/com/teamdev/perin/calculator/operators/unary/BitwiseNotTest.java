package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class BitwiseNotTest {

    @Test
    public void testUnaryMinusExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "~(2)";
        double expected = -3;
        double delta = 1e-100;
        assertEquals("Bitwise Not expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testSimpleExpressionWithUnaryMinus() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "5+~(2)";
        double expected = 2;
        double delta = 1e-100;
        assertEquals("Bitwise Not expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test (expected = ParseException.class)
    public void testFactorialByDouble() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "~1.2";
        calculator.evaluate(expression);
        fail("Bitwise Not argument must be integer. So exception expected. But any was thrown.");
    }

}


