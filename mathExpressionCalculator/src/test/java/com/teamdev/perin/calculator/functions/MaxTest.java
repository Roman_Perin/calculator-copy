package com.teamdev.perin.calculator.functions;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MaxTest {

    @Test
    public void testMaxExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "max(3, -1, 0)";
        double expected = 3;
        double delta = 1e-100;
        assertEquals("Max parsing is failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testSimpleExpressionWithMax() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "max(3+5, -1+1, 0-70)";
        double expected = 8;
        double delta = 1e-100;
        assertEquals("Max parsing is failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test (expected = ParseException.class)
    public void testMaxWithOneArgument() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "max(70)";
        calculator.evaluate(expression);
        fail("ParseException was expected, but evaluation was done without exception.");
    }

}
