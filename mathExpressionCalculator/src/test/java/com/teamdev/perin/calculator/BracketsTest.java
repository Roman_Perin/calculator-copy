package com.teamdev.perin.calculator;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class BracketsTest {

    private CalculatorImpl calculator;

    @Before
    public void setUp(){
        calculator = new CalculatorImpl();
    }

    @Test
    public void testSimpleBrackets() throws ParseException {
        String expression = "(1+2)*3";
        double expectedResult = 9;
        double delta = 1e-10;
        assertEquals("Brackets don't work.", expectedResult, calculator.evaluate(expression), delta);
    }

    @Test
    public void testComplexBrackets() throws ParseException {
        String expression = "((1+2)+(2+5*(1+6)))*3";
        double expectedResult = 120;
        double delta = 1e-10;
        assertEquals("Complex brackets don't work.", expectedResult, calculator.evaluate(expression), delta);
    }

    @Test (expected = ParseException.class)
    public void testUnclosedBracketException() throws ParseException {
        String expression = "1*(2+3";
        calculator.evaluate(expression);
    }

    @Test
    public void testUnclosedBracket() throws ParseException {
        String expression = "1*(2+3";
        int expectedPosition = 6;
        try {
            calculator.evaluate(expression);
            fail("ParseException was expected at position " + expectedPosition);
        } catch (ParseException e) {
            assertEquals("Error position defined incorrectly.", expectedPosition, e.getErrorPosition());
        }
    }

    @Test (expected = ParseException.class)
    public void testUnopenedBracketException() throws ParseException {
        String expression = "1*2)+3";
        calculator.evaluate(expression);
    }

    @Test
    public void testUnopenedBracket() throws ParseException {
        String expression = "1*2+3)";
        int expectedPosition = 6;
        try {
            calculator.evaluate(expression);
            fail("ParseException was expected at position " + expectedPosition);
        } catch (ParseException e) {
            assertEquals("Error position defined incorrectly.", expectedPosition, e.getErrorPosition());
        }
    }

}
