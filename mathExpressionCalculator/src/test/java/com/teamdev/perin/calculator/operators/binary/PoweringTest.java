package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import com.teamdev.perin.calculator.operators.Priority;
import org.junit.Before;
import org.junit.Test;

import static com.teamdev.perin.calculator.operators.Priority.POWERING_LEVEL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PoweringTest {

    private PoweringOperator powering;
    private Priority priority;

    @Before
    public void setUp()  {
        priority = POWERING_LEVEL;
        powering = new PoweringOperator(priority);
    }

    @Test
    public void testPoweringEvaluation() {
        double leftOperand = 10;
        double rightOperand = 2;
        double inaccuracy = 1e-10;
        double expected = 100d;
        assertEquals("Powering operator failed.", expected, powering.evaluate(leftOperand, rightOperand), inaccuracy);
    }

    @Test
    public void testPoweringExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "10^2";
        double expected = 100;
        double delta = 1e-100;
        assertEquals("Powering expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testPoweringCompareToItself() {
        PoweringOperator otherPowering = new PoweringOperator(priority);
        assertTrue("Something is wrong with powering comparing.", 0 == powering.compareTo(otherPowering));
    }

}
