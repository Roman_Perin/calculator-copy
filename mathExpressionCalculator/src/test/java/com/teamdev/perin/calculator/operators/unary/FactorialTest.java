package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FactorialTest {

    @Test
    public void testFactorialExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "3!";
        double expected = 6;
        double delta = 1e-100;
        assertEquals("Factorial expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test (expected = ParseException.class)
    public void testFactorialByDouble() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "-1.2!";
        calculator.evaluate(expression);
        fail("Factorial argument must be non-negative integer. So exception expected. But any was thrown.");
    }

    @Test
    public void testDoubleFactorialExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "9!!";
        double expected = 945;
        double delta = 1e-100;
        assertEquals("Factorial expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testThreefoldFactorialExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "9!!!";
        double expected = 162;
        double delta = 1e-100;
        assertEquals("Factorial expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testSimpleExpressionWithFactorial() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "1+(1+2)!";
        double expected = 7;
        double delta = 1e-100;
        assertEquals("Factorial expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

}
