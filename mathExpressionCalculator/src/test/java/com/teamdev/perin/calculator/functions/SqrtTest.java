package com.teamdev.perin.calculator.functions;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SqrtTest {

    @Test
    public void testSqrtExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "sqrt(9)";
        double expected = 3;
        double delta = 1e-100;
        assertEquals("Sqrt parsing is failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testSimpleExpressionWithSqrt() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "sqrt(3+6)";
        double expected = 3;
        double delta = 1e-100;
        assertEquals("Sqrt parsing is failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test (expected = ParseException.class)
    public void testSqrtWithTwoArguments() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "sqrt(9, 16)";
        calculator.evaluate(expression);
        fail("ParseException was expected, but evaluation was done without exception.");
    }

    @Test (expected = ParseException.class)
    public void testSqrtWithNegativeArguments() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "sqrt(-1)";
        calculator.evaluate(expression);
        fail("ParseException was expected, but evaluation was done without exception.");
    }

}
