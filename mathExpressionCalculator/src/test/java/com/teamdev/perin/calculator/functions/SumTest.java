package com.teamdev.perin.calculator.functions;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SumTest {

    @Test
    public void testSumExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "sum(3, -1, 0)";
        double expected = 2;
        double delta = 1e-100;
        assertEquals("Sum parsing is failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testSimpleExpressionWithSum() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "sum(3+5, -1+1, 0-70)";
        double expected = -62;
        double delta = 1e-100;
        assertEquals("Sum parsing is failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test (expected = ParseException.class)
    public void testSumWithOneArgument() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "sum(70)";
        calculator.evaluate(expression);
        fail("ParseException was expected, but evaluation was done without exception.");
    }

}
