package com.teamdev.perin.calculator;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Test;

import static org.junit.Assert.fail;

public class DelimitersTest {

    CalculatorImpl calculator = new CalculatorImpl();


    @Test(expected = ParseException.class)
    public void testCommaDelimiter() throws ParseException {
        String expression = "1,2";
        calculator.evaluate(expression);
        fail("Exception must to be. Delimiter \'comma\' is invalid externally the functions.");
    }

    @Test(expected = ParseException.class)
    public void testCommaDelimiterInsideBrackets() throws ParseException {
        String expression = "(1,2)";
        calculator.evaluate(expression);
        fail("Exception must to be. Delimiter \'comma\' is invalid externally the functions.");
    }

}
