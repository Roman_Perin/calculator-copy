package com.teamdev.perin.calculator.operators.binary;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import com.teamdev.perin.calculator.operators.Priority;
import org.junit.Before;
import org.junit.Test;

import static com.teamdev.perin.calculator.operators.Priority.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PlusOperatorTest {

    private PlusOperator plus;
    private Priority priority;

    @Before
    public void setUp() {
        priority = ADDITION_LEVEL;
        plus = new PlusOperator(priority);
    }

    @Test
    public void testPlusEvaluation() {
        double leftOperand = 10;
        double rightOperand = 2;
        double inaccuracy = 1e-10;
        double expected = 12d;
        assertEquals("Plus operator evaluates incorrectly.", expected, plus.evaluate(leftOperand, rightOperand), inaccuracy);
    }

    @Test
    public void testPlusExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "10+2";
        double expected = 12;
        double delta = 1e-100;
        assertEquals("Plus expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testPlusCompareToItself() {
        PlusOperator otherPlus = new PlusOperator(priority);
        assertTrue("Something is wrong with multiply comparing.", 0 == plus.compareTo(otherPlus));
    }

    @Test
    public void testPlusCompareToMultiplication() {
        Priority multiplyPriority = MULTIPLICATION_LEVEL;
        MultiplicationOperator multiply = new MultiplicationOperator(multiplyPriority);
        assertTrue("Something is wrong with plus and multiplication comparing." ,0 > plus.compareTo(multiply));
        assertTrue("Something is wrong with plus and multiplication comparing.", 0 < multiply.compareTo(plus));
    }

    @Test
    public void testPlusCompareToDivision() {
        Priority divisionPriority = MULTIPLICATION_LEVEL;
        DivisionOperator division = new DivisionOperator(divisionPriority);
        assertTrue("Something is wrong with plus and division comparing." ,0 > plus.compareTo(division));
        assertTrue("Something is wrong with plus and division comparing.", 0 < division.compareTo(plus));
    }

    @Test
    public void testPlusCompareToPowering() {
        Priority poweringPriority = POWERING_LEVEL;
        PoweringOperator powering = new PoweringOperator(poweringPriority);
        assertTrue("Something is wrong with plus and division comparing." ,0 > plus.compareTo(powering));
        assertTrue("Something is wrong with plus and division comparing.", 0 < powering.compareTo(plus));
    }
}
