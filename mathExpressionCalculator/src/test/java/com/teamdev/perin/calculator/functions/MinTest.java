package com.teamdev.perin.calculator.functions;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MinTest {

    @Test
    public void testMinExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "min(3, -1, 0)";
        double expected = -1;
        double delta = 1e-100;
        assertEquals("Min parsing is failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testSimpleExpressionWithMin() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "min(3+5, -1+1, 0-70)";
        double expected = -70;
        double delta = 1e-100;
        assertEquals("Min parsing is failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test (expected = ParseException.class)
    public void testMinWithOneArgument() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "min(70)";
        calculator.evaluate(expression);
        fail("ParseException was expected, but evaluation was done without exception.");
    }

}
