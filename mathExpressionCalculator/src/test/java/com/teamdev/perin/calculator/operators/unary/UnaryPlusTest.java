package com.teamdev.perin.calculator.operators.unary;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UnaryPlusTest {

    @Test
    public void testUnaryPlusExpression() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "+(2)";
        double expected = 2;
        double delta = 1e-100;
        assertEquals("Unary plus expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }

    @Test
    public void testSimpleExpressionWithUnaryPlus() throws ParseException {
        CalculatorImpl calculator = new CalculatorImpl();
        String expression = "+5+-(+2)";
        double expected = 3;
        double delta = 1e-100;
        assertEquals("Unary plus expression parsing failed.", expected, calculator.evaluate(expression), delta);
    }
}
