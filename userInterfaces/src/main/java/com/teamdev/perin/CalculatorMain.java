package com.teamdev.perin;

import com.teamdev.perin.consol.ConsoleCalculator;
import com.teamdev.perin.desktop.DesktopCalculator;

public class CalculatorMain {

    public static void main(String[] args) {

        if (args.length > 0 && args[0].equalsIgnoreCase("-c")){
            new ConsoleCalculator().run();
        } else {
            new DesktopCalculator().run();
        }

    }
}
