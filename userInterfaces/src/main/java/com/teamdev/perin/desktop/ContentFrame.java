package com.teamdev.perin.desktop;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import static com.teamdev.perin.calculator.util.ClassNameGetter.getCurrentClassName;

public class ContentFrame extends JFrame{

    private static Logger logger = LoggerFactory.getLogger(getCurrentClassName());

    private final int MIN_WIDTH = 600;
    private final int MIN_HEIGHT = 150;
    private String defaultErrorMessage = " ";
    private String startMessage = "Enter expression";
    private String evaluateButtonLabel = "=";

    private JTextArea inputText = new JTextArea(startMessage);
    private JLabel errorMessage = new JLabel(defaultErrorMessage);
    private JButton evaluate = new JButton(evaluateButtonLabel);
    private JTextField resultView = new JTextField();



    public ContentFrame() throws HeadlessException {
        super();

        setTitle("Calculator");
        setSize(new Dimension(MIN_WIDTH, MIN_HEIGHT));

        GridBagLayout gridGab = new GridBagLayout();
        setLayout(gridGab);

        initialization();

        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
    }


    private void initialization(){
        initializeInputTextArea();
        initializeErrorMessage();
        initializeEvaluateButton(initializeResultView());
    }


    private JLabel initializeErrorMessage() {

        int fillOption = GridBagConstraints.HORIZONTAL;
        int alignment = GridBagConstraints.NORTH;

        add(errorMessage, new GridBagConstraintsBuilder().cellCoordinates(0, 1).numberOfColumnsPerCell(2).
            numberOfRowsPerCell(1).fill(fillOption).insetFromTop(1).insetFromLeft(15).insetFromBottom(1).
            insetFromRight(15).anchor(alignment).build());

        return errorMessage;
    }


    private JTextField initializeResultView() {

        int fillOption = GridBagConstraints.HORIZONTAL;
        int alignment = GridBagConstraints.LINE_START;
        int fieldLength = 20;

        add(resultView, new GridBagConstraintsBuilder().cellCoordinates(1, 2).anchor(alignment)
                .fill(fillOption).insetFromTop(1).insetFromLeft(1).insetFromBottom(15).insetFromRight(15).build());

        resultView.setColumns(fieldLength);

        return resultView;
    }


    private JTextArea initializeInputTextArea() {

        int fillOption = GridBagConstraints.BOTH;

        add(inputText, new GridBagConstraintsBuilder().cellCoordinates(0, 0).numberOfColumnsPerCell(2).
            numberOfRowsPerCell(1).fill(fillOption).insetFromTop(15).insetFromLeft(15).insetFromBottom(1).
            insetFromRight(15).build());

        inputText.setLineWrap(true);
        inputText.setWrapStyleWord(true);

        inputText.addCaretListener(new CaretListener() {
            @Override
            public void caretUpdate(CaretEvent e) {
                ContentFrame.this.pack();
                ContentFrame.this.revalidate();
                errorMessage.setText(defaultErrorMessage);
            }
        });

        inputText.grabFocus();
        inputText.selectAll();

        return inputText;
    }


    private JButton initializeEvaluateButton(JComponent associatedComponent) {

        int fillOption = GridBagConstraints.HORIZONTAL;
        int width = associatedComponent.getPreferredSize().width;
        int height = associatedComponent.getPreferredSize().height;

        add(evaluate, new GridBagConstraintsBuilder().cellCoordinates(0, 2).weightX(0).weightY(0).
            fill(fillOption).insetFromTop(1).insetFromLeft(15).insetFromBottom(15).insetFromRight(1).build());

        if (width != 0 && height != 0) {
            evaluate.setPreferredSize(new Dimension(width / 2, height));
            evaluate.setMinimumSize(new Dimension(width / 4, height));
        }

        evaluate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    double result = new CalculatorImpl().evaluate(inputText.getText());
                    resultView.setText(Double.toString(result));
                    inputText.selectAll();
                    inputText.grabFocus();
                    pack();
                } catch (ParseException e) {
                    logger.debug("Exception was occurred during parsing." ,e);
                    inputText.setCaretPosition(e.getErrorPosition());
                    errorMessage.setText(e.getMessage());
                    inputText.grabFocus();
                    pack();
                }
            }
        });

        evaluate.setMnemonic(KeyEvent.VK_ENTER);
        evaluate.setToolTipText("Alt + Enter");

        return evaluate;
    }

}
