package com.teamdev.perin.consol;

import com.teamdev.perin.calculator.api.CalculatorImpl;
import com.teamdev.perin.calculator.api.ParseException;

import java.util.Scanner;

public class ConsoleCalculator {

    public void run(){

        Scanner in = new Scanner(System.in);

        boolean flag =  true;
        while (flag) {
            System.out.println("Please, enter expression: \n");
            String expression = in.nextLine();

            try {
                double result = new CalculatorImpl().evaluate(expression);
                System.out.println("result = " + result + "\n");
            } catch (ParseException e) {
                System.out.println(e.getMessage() + "\n" + "The mistake was occurred after "
                                   + e.getErrorPosition() + "-th character \n");
            }

            System.out.println("Do You want to re-enter expression? \n"
                               + "type in \"N\" - to exit, or \n"
                               + "press \'Enter\' to evaluateOperator another expression.");

            if (in.nextLine().equalsIgnoreCase("N")) {
                flag = false;
            }
        }

        in.close();
    }
}
