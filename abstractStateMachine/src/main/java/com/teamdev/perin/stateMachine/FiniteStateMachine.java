package com.teamdev.perin.stateMachine;

import java.util.Set;


public abstract class FiniteStateMachine<
        Result,
        State extends Enum,
        Context extends StateMachineContext<Result, Error>,
        Error extends Exception> {


    public Result run(Context context) throws Error {

        final TransitionMatrix<State> matrix = getTransitionMatrix();

        State currentState = matrix.getStartState();
        final State finishState = matrix.getFinishState();

        while (currentState != finishState) {

            final State newState = moveForward(currentState, context);

            if (newState == null) {
                deadlock(currentState, context);
            } else {
                currentState = newState;
            }
        }

        return context.getResult();
    }


    private State moveForward(State currentState, Context context) throws Error {
        final TransitionMatrix<State> matrix = getTransitionMatrix();
        final StateAcceptor<Result, State, Context, Error> stateAcceptor = createStateAcceptor();
        final Set<State> possibleStates = matrix.getPossibleStates(currentState);

        for (State state : possibleStates) {
            if (stateAcceptor.accept(state, context)) {
                return state;
            }
        }
        return null;
    }

    protected abstract void deadlock(State state, Context context) throws Error;

    protected abstract TransitionMatrix<State> getTransitionMatrix();

    protected abstract StateAcceptor<Result, State, Context, Error> createStateAcceptor();

}
