package com.teamdev.perin.stateMachine;

public interface StateMachineContext<Result, Error extends Exception> {

    public abstract Result getResult() throws Error;

}
