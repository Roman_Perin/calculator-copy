package com.teamdev.perin.stateMachine;

public interface StateAcceptor<
        Result,
        State extends Enum,
        Context extends StateMachineContext<Result, Error>,
        Error extends Exception> {

    boolean accept(State state, Context context) throws Error;

}
